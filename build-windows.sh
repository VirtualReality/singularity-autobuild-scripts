#!/bin/bash

set -x

if [ "$1" = "64" ]
then
    echo "Building Windows 64"
    VC="VC110"
    MFLAG="-m64"
    WINDOWS="windows64"
else
    echo "Building Windows 32"
    VC="VC100"
    MFLAG=""
    WINDOWS="windows"
fi    

WKDIR="$(realpath $(dirname $0))"
WINPY="/cygdrive/c/Python26/python"
CYGPY="/usr/bin/python"
SOURCE="$WKDIR/../singularity"
SOURCE="$(realpath $SOURCE)"
LOGFILE="$WKDIR/build.log"
URL="http://files.streamgrid.net/singularity"
SSH_HOST="ek@files.streamgrid.net"
REMOTE_ROOT="/var/www/singularity"
SCPDEST="$SSH_HOST:$REMOTE_ROOT"
CHAN="Singularity Alpha"
CHAN_NO_SPACE=${CHAN/ /}
SYMDEST="ek@crash.singularityviewer.org:/data/virtual/crash.singularityviewer.org/incoming_symbols"

ircmsg() {
    set +x
    $CYGPY $WKDIR/lolabot.py -a $IRCPASSWD -c \#SingularityViewer $@
    set -x
}

short_url () {
    curl -d "url=${1}" -s http://tinyurl.com/api-create.php
}

apply_patches() {
	cd $SOURCE/indra
	for i in $WKDIR/patches/*.patch; do
		patch --binary -p2 < $i
	done
}

vnum() {
    grep "$1" "$SOURCE/indra/llcommon/llversionviewer.h" | cut -f2 -d= | sed -e "s%[^0-9]%%g"
}

set_version() {
    cd $SOURCE
    VMAJOR=$(vnum LL_VERSION_MAJOR)
    VMINOR=$(vnum LL_VERSION_MINOR)
    VPATCH=$(vnum LL_VERSION_PATCH)
    VBUILD=$(vnum LL_VERSION_BUILD)
    VERSION="$VMAJOR.$VMINOR.$VPATCH.$VBUILD"
    HASH=$(git rev-list --max-count 1 HEAD)
}

prepare_build() {
    cd $SOURCE
    git reset --hard
    git clean -dxf
    git pull
    # apply_patches
    cd $SOURCE/indra
    rm -rf build-VC*
    $WINPY develop.py $MFLAG --unattended -t Release -G $VC configure -DUNATTENDED:BOOL=ON -DRELEASE_CRASH_REPORTING:BOOL=ON -DPACKAGE:BOOL=ON -DLL_TESTS:BOOL=OFF -DFMODEX:BOOL=ON -DVIEWER_CHANNEL:STRING="$CHAN" -DVIEWER_LOGIN_CHANNEL:STRING="$CHAN"
}

perform_build() {
    cd $SOURCE/indra
    $WINPY develop.py $MFLAG --unattended -t Release -G $VC build
}

check_result() {
    if [ -f $SOURCE/indra/build-$VC/newview/Release/${CHAN_NO_SPACE}_*_Setup.exe ]; then
        echo "Build success"
        installer=$(ls $SOURCE/indra/build-$VC/newview/Release/${CHAN_NO_SPACE}_*_Setup.exe)
        xpath=${installer%/*} 
        xbase=${installer##*/}
        xfext=${xbase##*.}
        xpref=${xbase%.*}
        cd "${SOURCE}/indra/build-$VC/newview/Release/"
        7z u secondlife-bin.pdb.7z secondlife-bin.pdb
        scp -q "${SOURCE}/indra/build-$VC/newview/Release/secondlife-bin.pdb.7z" "$SYMDEST/${CHAN_NO_SPACE}-${VERSION}-symbols-$WINDOWS.PDB.7z"
        scp -q "${SOURCE}/indra/build-$VC/newview/Release/secondlife-symbols-windows.tar.bz2" "$SYMDEST/${CHAN_NO_SPACE}-${VERSION}-symbols-$WINDOWS.tar.bz2"
        scp -q $installer ${SCPDEST}
        scp -q $LOGFILE "${SCPDEST}/${xbase}.log"
        log=$(short_url "$URL/${xbase}.log")
        ssh "$SSH_HOST" "$REMOTE_ROOT/lib/import_revs.php '${CHAN_NO_SPACE}' '${VERSION}' '${HASH}'"
        ssh "$SSH_HOST" "chmod 0644 $REMOTE_ROOT/*.exe $REMOTE_ROOT/*.log"
        ircmsg -t "BUILD" "$WINDOWS build success. Log $log installer $URL/${xbase}"
    else
        echo "Build failed"
        scp -q $LOGFILE ${SCPDEST}
        log=$(short_url "$URL/${LOGFILE##*/}")
        ssh "$SSH_HOST" "chmod 0644 $REMOTE_ROOT/*.exe $REMOTE_ROOT/*.log"
        ircmsg -t "BUILD" "Windows build failed. Log $log"
    fi
}

cd $WKDIR

set -o pipefail
set +x
IRCPASSWD=$(cat ircpasswd) 
 IRCPASSWD=${IRCPASSWD/[ 
]*/}
set -x

exec > >(tee $LOGFILE) 2>&1

echo "Starting new build"
ircmsg -t "BUILD" "Starting $WINDOWS build ${CHAN}."
prepare_build
set_version
perform_build
check_result

#!/bin/bash

set -x
WKDIR="$(realpath $(dirname $0))"
WINPY="/cygdrive/c/Python26/python"
CYGPY="/usr/bin/python"

ircmsg() {
    set +x
    $CYGPY $WKDIR/lolabot.py -a $IRCPASSWD -c \#SingularityViewer $@
    set -x
}

short_url () {
    curl -d "url=${1}" -s http://tinyurl.com/api-create.php
}

cd $WKDIR

set -o pipefail
set +x
IRCPASSWD=$(cat ircpasswd) 
 IRCPASSWD=${IRCPASSWD/[ 
]*/}
set -x

while true
do

    $CYGPY $WKDIR/checkbuild.py
    
    if [ -f $WKDIR/do_build ]; then
        echo "Starting new build"
        $WKDIR/build-windows.sh
        $WKDIR/build-windows.sh 64
        $WKDIR/build-vm.sh
        
        # post build
        ssh melisa.streamgrid.net /var/www/singularity/lib/sync-sf.sh
        curl -s  http://crash.singularityviewer.org/sync_builds.php
        rm -f $WKDIR/do_build
    fi

sleep 300

done
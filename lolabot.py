#!/usr/bin/env python

import re 
import string 
import sys 
import httplib 
import urllib
import urllib2 
import getopt
import os
   
def httpPost(postUrl, **args):
    req = urllib2.Request(postUrl)
    
    if args:
        data = urllib.urlencode(args)
        req.add_data(urllib.urlencode(args))
    
    try:
        f = urllib2.urlopen(req)
        res = f.read()
        f.close()
        return res
    except:
        return ""
    
def ircNotify(chan, auth, msg, topic = "", topicBold = False, topicColor = None):
    
    fullMsg = msg
    
    if topic:
        prepend = "["
        append = "]"
        if topicBold:
            prepend += "\x02"
            append += "\x02"
        if topicColor:
            prepend += "\x03" + str(topicColor)
            append += "\x03"
        fullMsg = prepend + topic + append[::-1] + " " + msg

    return httpPost("http://lola.radegast.org/~lkalif/lolabot.php",
             chan = chan,
             auth = auth,
             msg = fullMsg
            )
    
def usage(msg = "", exitCode = 0):
    print "LolaBot simple command line injector v 1.0"
    print "Copyright (c) 2012 Latif Khalifa"
    print
    if msg:
        print msg
        print
    print "Usage: %s -a <auth_code> -c <channel> [-t <topic>] [-b] [--color <color[,background]>] message" % (sys.argv[0])
    print "-h --help     help"
    print "-a --auth     authorization token"
    print "-c --chan     irc channel"
    print "-t --topic    topic"
    print "-b --bold     topic in bold"
    print "--color f[,b] topic color, (optionally followed by with background color)"
    print "              (numerical value[s], 0-15)"
    sys.exit(exitCode)

def main():
    try:
        opts, args = getopt.getopt(sys.argv[1:], "ha:c:t:b", ["help", "auth=", "chan=", "topic=", "bold", "color="])
    except getopt.GetoptError, err:
        usage(str(err))
    
    bold = False
    auth = None
    chan = None
    topic = None
    topicBold = False
    topicColor = None
    
    for o, a in opts:
        if o in ("-h", "--help"):
            usage()
        elif o in ("-a", "--auth"):
            auth = a
        elif o in ("-c", "--chan"):
            chan = a
        elif o in ("-t", "--topic"):
            topic = a
        elif o in ("-b", "--bold"):
            topicBold = True
        elif o in ("--color"):
            topicColor = a

    
    if not auth:
        usage("Required option -a [--auth] missing", 2)
    
    if not chan:
        usage("Required option -c [--chan] missing", 2)
        
    msg = " ".join(args)
    
    if not msg:
        usage("Message missing", 2)
        
    ircNotify(chan, auth, msg, topic = topic, topicBold = topicBold, topicColor = topicColor)

if __name__ == '__main__':
    main()
    
#!/bin/bash

set -x
VMRUN="/cygdrive/c/Program Files (x86)/VMware/VMware Workstation/vmrun.exe"

check_vm() {
    res=`"$VMRUN" list | grep -- "$VM_NAME"`
    if [ "x$res" == "x" ]; then
        echo "Virtual machine not running, starting..."
        "$VMRUN" start "$VM_NAME" nogui
        sleep 30
    else
        echo "Virtual machine already running."
    fi
}

perform_build() {
    check_vm
    ssh $VM_IP "/singularity/autobuild/autobuild-cron.sh"
    ssh $VM_IP "sudo halt"
}

VM_NAME='D:\\disk\\vm\\vmware\\Debian 6 64-bit\\Debian 6 64-bit.vmx'
VM_IP=10.0.0.201

perform_build

VM_NAME='D:\\disk\\vm\\vmware\\Debian 6 32-bit\\Debian 6 32-bit.vmx'
VM_IP=10.0.0.202

perform_build

VM_NAME='D:\\disk\\vm\\vmware\\Mac OS X Server 10.6\\Mac OS X Server 10.6.vmx'
VM_IP=10.0.0.204

res=`"$VMRUN" list | grep -- "$VM_NAME"`
if [ "x$res" == "x" ]; then
    echo "Virtual machine not running, starting..."
    "$VMRUN" start "$VM_NAME" nogui
    sleep 90
else
    echo "Virtual machine already running."
fi

ssh $VM_IP "bash -l /singularity/autobuild/autobuild-cron.sh"
# "$VMRUN" suspend "$VM_NAME"
ssh $VM_IP "sudo halt"

#!/usr/bin/env winpy

import re 
import string 
import sys 
import httplib 
import urllib
import urllib2 
from xml.dom import minidom
import getopt
import json
import os
import socket

config_file = "rss_build_checker"

def getLastCommit():
    try:
        f = open(config_file, "r")
        l = f.readline().strip()
        f.close()
        return l
    except:
        return None

def saveLastCommit(date):
    try:
        f = open(config_file, "w")
        f.write(date)
        f.close()
    except:
        pass

def getText(element, field):
    try:
        return element.getElementsByTagName(field)[0].childNodes[0].data.strip()
    except:
        return ""
    
def getRss(url):
    try:
        print "Fetching from %s" %(url)
        file_request = urllib2.Request(url) 
        file_opener = urllib2.build_opener() 
        file_feed = file_opener.open(file_request, timeout=30).read() 
        file_xml = minidom.parseString(file_feed) 
        last_commit = file_xml.getElementsByTagName("entry")[0]
        title = getText(last_commit, "title")
        date = getText(last_commit, "updated").strip()
        print "Last commit \"%s\" on %s" % (title, date)
        return date
    except:
        return None

def exitWithError(msg, errorCode = 1):
    print msg
    sys.exit(errorCode)
    
def httpPost(postUrl, **args):
    req = urllib2.Request(postUrl)
    
    if args:
        data = urllib.urlencode(args)
        req.add_data(urllib.urlencode(args))
    
    try:
        f = urllib2.urlopen(req)
        res = f.read()
        f.close()
        return res
    except:
        return ""
    
def shortUrl(url):
    res = httpPost("http://tinyurl.com/api-create.php", url = url)
    if not res: return ""
    return str(res)

def ircNotify(chan, msg):
    httpPost("http://nimitz.openmetaverse.org/~lkalif/lolabot.php",
             chan = chan,
             auth = "omf_rulez_2010",
             msg = msg
            )

def main():
    socket.setdefaulttimeout(30.0)
    feed_url = "https://github.com/singularity-viewer/SingularityViewer/commits/master.atom"
    
    newCommit = getRss(feed_url)
    if not newCommit:
        exitWithError("Failed to fetch the RSS feed")
        
    lastCommit = getLastCommit()
    saveLastCommit(newCommit)
    
    if newCommit != lastCommit:
        print "New commit detected"
        try:
            if not os.path.exists("do_build"):
                open("do_build", "w").close()
            os.utime("do_build", None)
        except Exception as ex:
            print "WARNING: %s" % (ex)
    else:
        print "Nothing to do"

if __name__ == '__main__':
    main()
